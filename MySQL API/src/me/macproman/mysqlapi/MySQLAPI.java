package me.macproman.mysqlapi;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySQLAPI {

	public void onEnable() {
		System.out.print("MySQL API coded by Myles / MylesLovesMemes");
	}

	public void onDisable() {
		// INFO: For neatness.
	}

	static Connection c;

	public static void connect(String host, String user, String pass) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.print("Attempting to connect to database...");

			c = DriverManager.getConnection(host, user, pass);
			System.out.print("Connected to database.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
